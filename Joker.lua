--修复区域显示问题
local recovery_show = C_BattleNet.GetFriendGameAccountInfo
C_BattleNet.GetFriendGameAccountInfo = function(...)
local gameAccountInfo = recovery_show(...)
gameAccountInfo.isInCurrentRegion = true
return gameAccountInfo;
end
